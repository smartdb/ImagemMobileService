﻿using ImagemMobileService.Models.Enum;
using ImagemMobileService.Models.LogOcorrencias;
using ImagemMobileService.Models.Parametros;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImagemMobileService.Models
{
    partial class ImagemMobile : ServiceBase
    {
        private System.Timers.Timer timer;

        private double tempo;

        private Semaphore Pool;

        private static bool flag = false;

        public ParametrosEntity parametro;

        public string Detalhe = string.Empty;
        public ImagemMobile()
        {
            InitializeComponent();

            if (!System.Diagnostics.EventLog.SourceExists("ImagemMobileServiceLogSourse"))
                System.Diagnostics.EventLog.CreateEventSource("ImagemMobileServiceLogSourse",
                                                                      "ImagemMobileServiceLog");

            eventLog1.Source = "ImagemMobileLogSourse";
            // the event log source by which 
            //the application is registered on the computer
            eventLog1.Log = "ImagemMobileServiceLog";

            parametro = new ParametrosBusiness().RetornaParametrosKaptor((int)Kaptor.ImagemMobile);

            tempo = parametro.Tempo.TotalMilliseconds;
        }

        protected override void OnStart(string[] args)
        {
            this.timer = new System.Timers.Timer(tempo);  // 600000 milliseconds = 10 minutes//14400000 miliseconds = 4 hours
            this.timer.AutoReset = true;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
            this.timer.Start();
            eventLog1.WriteEntry("ImagemMobileService iniciou");
        }

        protected override void OnStop()
        {
            this.timer.Stop();
            this.timer = null;
            eventLog1.WriteEntry("ImagemMobileService parou");
        }

        protected override void OnContinue()
        {
            eventLog1.WriteEntry("ImagemMobileService está em execução");
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                new ImagemMobileBusiness().IncluirImagensMobileSalvados();
                //eventLog1.WriteEntry("ok");
                //sn.Close();
                new ParametrosBusiness().AtualizarDataExecucao((int)Kaptor.ImagemMobile, "N", Detalhe);


                eventLog1.WriteEntry("Executando Serviço...");
                

            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry(ex.Message + "|" + ex.Source);
                eventLog1.WriteEntry(ex.Data + "|" + ex.StackTrace + "|");

                new ParametrosBusiness().AtualizarDataExecucao((int)Kaptor.ImagemMobile, "S", ex.Message);

                new LogOcorrenciasBusiness().Incluir(new LogOcorrenciasEntity((int)Kaptor.ImagemMobile,
                                                                                  ex.Message,
                                                                                  "",
                                                                                  "",
                                                                                  "",
                                                                                  "",
                                                                                  ""));

            }
        }


        //public void Iniciar()
        //{
        //    try
        //    {
        //        //flag = true;
        //        //if (!IsProcessOpen("ImportacaoRoteirosPAI"))
        //        //{
        //        string applicationName = AppDomain.CurrentDomain.BaseDirectory;

        //        applicationName = applicationName.Replace(@"ImportacaoRoteirosPAI.Service\bin\Debug\", @"ImportacaoRoteirosPAI\bin\Debug\ImportacaoRoteirosPAI.exe");

        //        Process objprocess = new Process();

        //        objprocess.StartInfo.FileName = applicationName;

        //        objprocess.Start();

        //        //ApplicationLoader.PROCESS_INFORMATION procInfo;
        //        //ApplicationLoader.StartProcessAndBypassUAC(applicationName, out procInfo);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    //finally
        //    //{
        //    //    Pool.Release();

        //    //    flag = false;
        //    //}


        //}
    }
}
