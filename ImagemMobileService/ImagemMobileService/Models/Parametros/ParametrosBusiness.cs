﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models.Parametros
{
    public class ParametrosBusiness
    {
        #region Consultas

        public ParametrosEntity RetornaParametrosKaptor(int IdTipoKaptor)
        {
            return new ParametrosDao().RetornaParametrosKaptor(IdTipoKaptor);
        }

        #endregion Consultas

        #region Manutenção

        public void AtualizarDataExecucao(int IdTipoKaptor, string IndErro, string Detalhe)
        {
            new ParametrosDao().AtualizarDataExecucao(IdTipoKaptor, IndErro, Detalhe);
        }

        #endregion
    }
}
