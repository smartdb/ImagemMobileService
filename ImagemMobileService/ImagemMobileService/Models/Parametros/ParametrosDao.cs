﻿using System;
using System.Data;
using System.Data.SqlClient;
using ImagemMobileService.Models.Conexao;

namespace ImagemMobileService.Models.Parametros
{
    public class ParametrosDao
    {
        #region Consultas

        public ParametrosEntity RetornaParametrosKaptor(int IdTipoKaptor)
        {
            ParametrosEntity retorno = new ParametrosEntity();

            try
            {
                SqlConnection conn = new AcessoBancoDados().OpenConnection();
                SqlCommand comm = new SqlCommand();

                DataSet dsRetorno = new DataSet();

                SqlDataAdapter sda = new SqlDataAdapter(comm);

                comm.Connection = conn;
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "SPVERITHUS_RetornaParametrosKaptor";
                comm.Parameters.AddWithValue("@IdTipoKaptor", IdTipoKaptor);

                new AcessoBancoDados().OpenConnection();


                sda.Fill(dsRetorno);

                new AcessoBancoDados().CloseConnection();

                if (dsRetorno.Tables[0].Rows.Count > 0)
                {
                    DataRow Linha = dsRetorno.Tables[0].Rows[0];

                    retorno.Tempo = (TimeSpan)(Linha["Tempo"]);

                }
            }
            catch (Exception e)
            {
                //retorno.codErro = 99;
                //retorno.msgErro = e.Message;
            }

            return retorno;
        }

        #endregion Consultas

        #region Manutenção

        public void AtualizarDataExecucao(int IdTipoKaptor, string IndErro, string Detalhe)
        {
            try
            {
                SqlConnection conn = new AcessoBancoDados().OpenConnection();
                SqlCommand comm = new SqlCommand();


                comm.Connection = conn;
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "SPVERITHUS_AtualizarDataExecucaoKaptor";

                comm.Parameters.AddWithValue("@IdTipoKaptor", IdTipoKaptor);
                comm.Parameters.AddWithValue("@IndErro", IndErro);
                comm.Parameters.AddWithValue("@Detalhe", Detalhe);

                new AcessoBancoDados().OpenConnection();

                comm.ExecuteScalar();

                new AcessoBancoDados().CloseConnection();
            }
            catch (Exception e)
            {
                throw e;
            }

        }



        #endregion
    }
}
