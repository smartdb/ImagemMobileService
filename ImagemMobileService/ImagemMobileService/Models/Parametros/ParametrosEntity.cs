﻿using System;

namespace ImagemMobileService.Models.Parametros
{
    public class ParametrosEntity
    {

        #region Private members

        public TimeSpan Tempo { get; set; }

        #endregion Private members

        #region Construtor

        public ParametrosEntity()
        {

        }

        #endregion Construtor

    }
}
