﻿using ImagemMobileService.Models.ImagemVistoria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models
{
    public class ImagemMobileEntity
    {
        public int IdImagem { get; set; }

        public string Imagem { get; set; }

        public string NumPlaca { get; set; }


        public string IndIncluido { get; set; }

        public ImagemVistoriaEntity ImagemVistoria { get; set; }

        public ImagemMobileEntity()
        {
            ImagemVistoria = new ImagemVistoriaEntity();
        }

    }
}
