﻿using ImagemMobileService.Models.Enum;
using ImagemMobileService.Models.ImagemVeiculo;
using ImagemMobileService.Models.ImagemVistoria;
using ImagemMobileService.Models.LogOcorrencias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models
{
    public class ImagemMobileBusiness
    {
        public void IncluirImagensMobile()
        {
            try
            {
                IList<ImagemMobileEntity> LstImagemMobile = new List<ImagemMobileEntity>();

                LstImagemMobile = new ImagemMobileDao().RetornaImagens();

                string nomeimagem = string.Empty;
                //int cont = 0;

                foreach (var ImagemMobile in LstImagemMobile.OrderBy(x => x.NumPlaca))
                {
                    try
                    {
                        if (nomeimagem != ImagemMobile.NumPlaca)
                            nomeimagem = ImagemMobile.NumPlaca;


                        ImagemMobile.ImagemVistoria.ImagemVeiculo.Nome = nomeimagem + "_" + ImagemMobile.IdImagem + ".jpg";

                        ImagemMobile.ImagemVistoria.ImagemVeiculo.IdImagem = new ImagemVeiculoDao().IncluirImagemVeiculo(ImagemMobile.ImagemVistoria.ImagemVeiculo);

                        if (ImagemMobile.ImagemVistoria.ImagemVeiculo.IdImagem != 0)
                        {
                            new ImagemVistoriaDao().IncluirImagemVistoria(ImagemMobile.ImagemVistoria);

                            ImagemMobile.IndIncluido = "S";

                            new ImagemMobileDao().AlterarImagemMobile(ImagemMobile);

                            new LogOcorrenciasBusiness().Incluir(new LogOcorrenciasEntity((int)Kaptor.ImagemMobile,
                               "Sucesso na inclusão da imagem",
                               "",
                               "",
                               "",
                               "IdCliente|IdRoteiro|IdSinistro|IdVistoria|CodSinistro|NumPlaca",
                               ImagemMobile.ImagemVistoria.IdCliente + "|" + ImagemMobile.ImagemVistoria.IdRoteiro + "|" + ImagemMobile.ImagemVistoria.IdSinistro + "|" + ImagemMobile.NumPlaca));
                        }
                        else
                        {
                            throw new Exception("Não foi possivel incluir imagem");
                        }

                    }
                    catch (Exception ex)
                    {
                        ImagemMobile.IndIncluido = "E";

                        new LogOcorrenciasBusiness().Incluir(new LogOcorrenciasEntity((int)Kaptor.ImagemMobile,
                                "ERRO: " + ex.Message,
                                "",
                                "",
                                "",
                                "IdCliente|IdRoteiro|IdSinistro|IdVistoria|CodSinistro|NumPlaca",
                                ImagemMobile.ImagemVistoria.IdCliente + "|" + ImagemMobile.ImagemVistoria.IdRoteiro + "|" + ImagemMobile.ImagemVistoria.IdSinistro + "|" + ImagemMobile.NumPlaca));
                    }

                }
            }
            catch (Exception ex)
            {

                new LogOcorrenciasBusiness().Incluir(new LogOcorrenciasEntity((int)Kaptor.ImagemMobile,
                              "ERRO: " + ex.Message,
                               "",
                               "",
                               "",
                               "",
                               ""));
            }


        }


        public void IncluirImagensMobileSalvados()
        {
            try
            {
                IList<ImagemMobileEntity> LstImagemMobile = new List<ImagemMobileEntity>();

                LstImagemMobile = new ImagemMobileDao().RetornaImagensSalvados();



                foreach (var ImagemMobile in LstImagemMobile.OrderBy(x => x.NumPlaca))
                {
                    try
                    {

                        new ImagemVistoriaDao().IncluirImagemAuditoria(ImagemMobile.ImagemVistoria);

                        ImagemMobile.IndIncluido = "S";

                        new ImagemMobileDao().AlterarImagemMobile(ImagemMobile);

                        new LogOcorrenciasBusiness().Incluir(new LogOcorrenciasEntity((int)Kaptor.ImagemMobile,
                           "Sucesso na inclusão da imagem",
                           "",
                           "",
                           "",
                           "IdCliente|IdSinistro|IdLaudo|NumPlaca",
                           ImagemMobile.ImagemVistoria.IdCliente + "|" + ImagemMobile.ImagemVistoria.IdSinistro + "|" + ImagemMobile.ImagemVistoria.IdLaudo + "|" + ImagemMobile.NumPlaca));


                    }
                    catch (Exception ex)
                    {
                        ImagemMobile.IndIncluido = "E";

                        new LogOcorrenciasBusiness().Incluir(new LogOcorrenciasEntity((int)Kaptor.ImagemMobile,
                                "ERRO: " + ex.Message,
                                "",
                                "",
                                "",
                                "IdCliente|IdSinistro|IdLaudo|NumPlaca",
                                ImagemMobile.ImagemVistoria.IdCliente + "|" + ImagemMobile.ImagemVistoria.IdSinistro + "|" + ImagemMobile.ImagemVistoria.IdLaudo + "|" + ImagemMobile.NumPlaca));

                    }

                }
            }
            catch (Exception ex)
            {

                new LogOcorrenciasBusiness().Incluir(new LogOcorrenciasEntity((int)Kaptor.ImagemMobile,
                              "ERRO: " + ex.Message,
                               "",
                               "",
                               "",
                               "",
                               ""));
            }


        }
    }
}
