﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models.ImagemVeiculo
{
   public class ImagemVeiculoEntity
    {

        public int IdImagem { get; set; }

        public byte[] Imagem { get; set; }

        public string NumPlaca { get; set; }

        public string Nome { get; set; }

    }
}
