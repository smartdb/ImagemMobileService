﻿using ImagemMobileService.Models.ImagemVeiculo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models.ImagemVistoria
{
    public class ImagemVistoriaEntity
    {

        public int IdCliente { get; set; }

        public int IdSinistro { get; set; }

        public int IdRoteiro { get; set; }

        public int IdLaudo { get; set; }


        public ImagemVeiculoEntity ImagemVeiculo { get; set; }

        public ImagemVistoriaEntity()
        {
            ImagemVeiculo = new ImagemVeiculoEntity();
        }

    }
}
