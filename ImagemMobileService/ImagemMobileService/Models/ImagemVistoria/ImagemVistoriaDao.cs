﻿using ImagemMobileService.Models.Conexao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models.ImagemVistoria
{
    public class ImagemVistoriaDao
    {
         public void IncluirImagemVistoria(ImagemVistoriaEntity ImagemVistoria){
            // int retorno = 0;

             //string thisMsgErro = string.Empty;
             AcessoBancoDados acesso = new AcessoBancoDados();


             try
             {
                 SqlConnection conn = acesso.OpenConnection();
                 SqlCommand comm = new SqlCommand();

                 comm.Connection = conn;
                 comm.CommandType = CommandType.StoredProcedure;
                 comm.CommandText = "SPVERITHUS_IncluirImagensLaudoVistoria";


                 comm.Parameters.AddWithValue("@IdCliente", ImagemVistoria.IdCliente);
                 comm.Parameters.AddWithValue("@IdRoteiro", ImagemVistoria.IdRoteiro);
                 comm.Parameters.AddWithValue("@IdSinistro", ImagemVistoria.IdSinistro);
                 comm.Parameters.AddWithValue("@IdVistoria", 0);
                 comm.Parameters.AddWithValue("@IdImagem", ImagemVistoria.ImagemVeiculo.IdImagem);
                 comm.Parameters.AddWithValue("@IdUser", 26);

                 //object oIdentity = comm.ExecuteScalar();
                 //if (oIdentity != null)
                 //    retorno = Convert.ToInt32(oIdentity);

                 comm.ExecuteScalar();

                 acesso.CloseConnection();
             }
             catch (Exception ex)
             {
                 throw ex;
             }



             //return retorno;
         }

         public void IncluirImagemAuditoria(ImagemVistoriaEntity ImagemVistoria)
         {
             // int retorno = 0;

             //string thisMsgErro = string.Empty;
             AcessoBancoDados acesso = new AcessoBancoDados();


             try
             {
                 SqlConnection conn = acesso.OpenConnection();
                 SqlCommand comm = new SqlCommand();

                 comm.Connection = conn;
                 comm.CommandType = CommandType.StoredProcedure;
                 comm.CommandText = "SPVERITHUS_IncluirImagensLaudosAuditoria";


                 comm.Parameters.AddWithValue("@IdCliente", ImagemVistoria.IdCliente);
                 comm.Parameters.AddWithValue("@IdLaudo", ImagemVistoria.IdLaudo);
                 comm.Parameters.AddWithValue("@IdSinistro", ImagemVistoria.IdSinistro);
                 comm.Parameters.AddWithValue("@Imagem", ImagemVistoria.ImagemVeiculo.Imagem);
                 comm.Parameters.AddWithValue("@IdUser", 0);             

                 comm.ExecuteScalar();

                 acesso.CloseConnection();
             }
             catch (Exception ex)
             {
                 throw ex;
             }



             //return retorno;
         }


    }
}
