﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagemMobileService.Models.Conexao;

namespace ImagemMobileService.Models.LogOcorrencias
{
    public class LogOcorrenciasDao
    {
        public LogOcorrenciasEntity Incluir(LogOcorrenciasEntity LogOcorrencias)
        {
            try
            {
                //SqlConnection conn = new SqlConnection(@"Data Source=verithus.dyndns.org,2250\MSSQLSERVER_14;Initial Catalog=Verithus_Teste;User ID=verithus;Password=verithus");
                SqlConnection conn = new AcessoBancoDados().OpenConnection();
                SqlCommand comm = new SqlCommand();


                comm.Connection = conn;
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "SPVERITHUS_IncluirLogKaptor";

                comm.Parameters.AddWithValue("@IdTipoKaptor", LogOcorrencias.IdTipoKaptor);
                comm.Parameters.AddWithValue("@DscOcorrencia", LogOcorrencias.DscOcorrencia);
                comm.Parameters.AddWithValue("@DscChavePrimaria", LogOcorrencias.DscChavePrimaria);
                comm.Parameters.AddWithValue("@DscConteudoChavePrimaria", LogOcorrencias.DscConteudoChavePrimaria);
                comm.Parameters.AddWithValue("@DscCampos", LogOcorrencias.DscCampos);
                comm.Parameters.AddWithValue("@DscConteudo", LogOcorrencias.DscConteudo);
                comm.Parameters.AddWithValue("@IndAcao", LogOcorrencias.IndAcao);

                //new AcessoBancoDados().OpenConnection(conn);

                comm.ExecuteScalar();

                new AcessoBancoDados().CloseConnection();
            }
            catch (Exception e)
            {
                throw e;
            }
            return LogOcorrencias;
        }
    }
}
