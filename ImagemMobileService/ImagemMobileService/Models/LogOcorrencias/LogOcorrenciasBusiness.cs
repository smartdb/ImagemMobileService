﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using ImportacaoRoteirosPAI.Models.Email;
using ImagemMobileService.Models.Enum;


namespace ImagemMobileService.Models.LogOcorrencias
{
    public class LogOcorrenciasBusiness
    {
        #region Manutenção

        //public void IncluirLog(LogOcorrenciasEntity LogOcorrencias, DataRow Cabecalho, DataRow Linha)
        //{
        //    EmailEntity ObjEmail = new EmailEntity();

        //    Incluir(LogOcorrencias);

        //    ObjEmail.MsgErro = LogOcorrencias.DscOcorrencia;

        //    new EmailBusiness().Incluir(ObjEmail);
        //}

        public LogOcorrenciasEntity Incluir(LogOcorrenciasEntity LogOcorrencias)
        {
            return new LogOcorrenciasDao().Incluir(LogOcorrencias);
        }

        //public LogOcorrenciasEntity Incluir(LogOcorrenciasEntity LogOcorrencias)
        //{
        //    return new LogOcorrenciasDao().Incluir(LogOcorrencias);
        //}
        #endregion
    }
}
