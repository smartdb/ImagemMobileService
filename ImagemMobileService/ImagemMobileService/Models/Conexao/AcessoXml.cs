﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models.Conexao
{
    public class AcessoXml
    {
        public ConnectionString RetornaConnectionString(string name)
        {
            ConnectionString conn = new ConnectionString();
            string folder = AppDomain.CurrentDomain.BaseDirectory;
            string document = "ConexaoBancoDados.xml";
            System.Xml.XmlDataDocument oDoc = new System.Xml.XmlDataDocument();

            try
            {
                oDoc.Load(folder + "\\" + document);

                foreach (System.Xml.XmlNode oNode in oDoc.SelectNodes("//connectionStrings/add"))
                {
                    if (oNode.Attributes.GetNamedItem("name").InnerText.Equals("contingencia") && name.Equals("contingencia"))
                    {
                        conn.Name = oNode.Attributes.GetNamedItem("name").InnerText;

                        string[] lstSplit = oNode.Attributes.GetNamedItem("connectionString").InnerText.Split(';');

                        conn.DataSource = lstSplit[0].Replace("Data Source=", "");
                        conn.InitialCatalog = lstSplit[1].Replace("Initial Catalog=", "");
                        conn.User = lstSplit[2].Replace("User ID=", "");
                        conn.Password = lstSplit[3].Replace("Password=", "");
                    }

                    if (oNode.Attributes.GetNamedItem("name").InnerText.Equals("Excel") && name.Equals("Excel"))
                    {
                        conn.Name = oNode.Attributes.GetNamedItem("name").InnerText;

                        string[] lstSplit = oNode.Attributes.GetNamedItem("connectionString").InnerText.Split(';');

                        conn.DataSource = lstSplit[1].Replace("Data Source=", "");
                    }
                }
            }
            catch
            {
                throw new Exception("Erro ao conectar com o ConexaoBancoDados.xml");
            }

            return conn;
        }
    }
}
