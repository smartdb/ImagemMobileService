﻿//using ImagemMobileService.Models.ImagemMobile;
//using SalvaImagemNaPasta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImagemMobileService.Models
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //#if(!DEBUG)
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new ImagemMobile() 
            };
            ServiceBase.Run(ServicesToRun);
            //#else
            //            new ImagemMobileBusiness().IncluirImagensMobile();
            //            
            //#endif

        }
    }
}
